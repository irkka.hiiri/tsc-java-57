package ru.tsc.ichaplygina.taskmanager.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.ichaplygina.taskmanager.api.service.ISessionService;
import ru.tsc.ichaplygina.taskmanager.endpoint.Session;

@Getter
@Setter
@Service
@NoArgsConstructor
@AllArgsConstructor
public class SessionService implements ISessionService {

    @Nullable
    private Session session;

}
