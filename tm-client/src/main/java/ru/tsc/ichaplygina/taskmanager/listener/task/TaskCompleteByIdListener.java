package ru.tsc.ichaplygina.taskmanager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class TaskCompleteByIdListener extends AbstractTaskListener {

    @NotNull
    public final static String DESCRIPTION = "complete task by id";
    @NotNull
    public final static String NAME = "complete task by id";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskCompleteByIdListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final String id = readLine(ID_INPUT);
        getTaskEndpoint().completeTaskById(sessionService.getSession(), id);
    }

}
