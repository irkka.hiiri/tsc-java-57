package ru.tsc.ichaplygina.taskmanager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;
import ru.tsc.ichaplygina.taskmanager.listener.AbstractListener;

import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.APP_HELP_HINT_TEXT;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printList;

@Component
public final class HelpListener extends AbstractListener {

    @NotNull
    public final static String ARG_NAME = "-h";

    @NotNull
    public final static String CMD_NAME = "help";

    @NotNull
    public final static String DESCRIPTION = "show this message";

    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    @NotNull
    @Override
    public final String argument() {
        return ARG_NAME;
    }

    @NotNull
    @Override
    public final String command() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@helpListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println(APP_HELP_HINT_TEXT);
        printList(listeners);
    }

}
