package ru.tsc.ichaplygina.taskmanager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class TaskAddToProjectListener extends AbstractTaskListener {

    @NotNull
    public final static String DESCRIPTION = "add task to a project";
    @NotNull
    public final static String NAME = "add task to project";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskAddToProjectListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final String projectId = readLine(PROJECT_ID_INPUT);
        @NotNull final String taskId = readLine(TASK_ID_INPUT);
        getTaskEndpoint().addTaskToProject(sessionService.getSession(), projectId, taskId);
    }

}
