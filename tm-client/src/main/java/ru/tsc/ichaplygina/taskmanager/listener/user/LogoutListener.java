package ru.tsc.ichaplygina.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

@Component
public final class LogoutListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "logout from the system";
    @NotNull
    public static final String NAME = "logout";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@logoutListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        getSessionEndpoint().closeSession(sessionService.getSession());
        sessionService.setSession(null);
    }
}
