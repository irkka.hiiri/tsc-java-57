package ru.tsc.ichaplygina.taskmanager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;
import ru.tsc.ichaplygina.taskmanager.listener.AbstractListener;

import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@Component
public final class ListCommandsListener extends AbstractListener {

    @NotNull
    public final static String CMD_NAME = "list arguments";

    @NotNull
    public final static String DESCRIPTION = "list available command-line arguments";

    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    @Nullable
    @Override
    public final String argument() {
        return null;
    }

    @NotNull
    @Override
    public final String command() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@listCommandsListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println();
        for (@NotNull final AbstractListener listener : listeners) {
            if (isEmptyString(listener.argument())) continue;
            System.out.println(listener.argument());
        }
        System.out.println();
    }

}
