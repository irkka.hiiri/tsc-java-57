package ru.tsc.ichaplygina.taskmanager.listener.domain;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.listener.AbstractListener;

@Component
public abstract class AbstractDomainListener extends AbstractListener {

    @NotNull
    protected static final String APPLICATION_JSON = "application/json";
    @NotNull
    protected static final String FILE_BASE64 = "data.b64";
    @NotNull
    protected static final String FILE_BINARY = "data.bin";
    @NotNull
    protected static final String FILE_FASTERXML_JSON = "data.fasterxml.json";
    @NotNull
    protected static final String FILE_FASTERXML_XML = "data.fasterxml.xml";
    @NotNull
    protected static final String FILE_FASTERXML_YAML = "data.fasterxml.yaml";
    @NotNull
    protected static final String FILE_JAXB_JSON = "data.jaxb.json";
    @NotNull
    protected static final String FILE_JAXB_XML = "data.jaxb.xml";
    @NotNull
    protected static final String FILE_SAVED_DATA = "data.sav";
    @NotNull
    protected static final String JAVAX_XML_BIND_CONTEXT_FACTORY = "javax.xml.bind.context.factory";
    @NotNull
    protected static final String ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @Nullable
    public String argument() {
        return null;
    }

}
