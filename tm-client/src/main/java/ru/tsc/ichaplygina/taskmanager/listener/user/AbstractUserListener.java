package ru.tsc.ichaplygina.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.endpoint.User;
import ru.tsc.ichaplygina.taskmanager.listener.AbstractListener;

@Component
public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    protected final String ENTER_EMAIL = "Enter email: ";
    @NotNull
    protected final String ENTER_FIRST_NAME = "Enter first name: ";
    @NotNull
    protected final String ENTER_LAST_NAME = "Enter last name: ";
    @NotNull
    protected final String ENTER_LOGIN = "Enter login: ";
    @NotNull
    protected final String ENTER_MIDDLE_NAME = "Enter middle name: ";
    @NotNull
    protected final String ENTER_PASSWORD = "Enter password: ";
    @NotNull
    protected final String ENTER_ROLE = "Enter user role: (Admin, User; default: User) ";

    @Nullable
    public String argument() {
        return null;
    }

    protected void showUser(@NotNull final User user) {
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Role: " + user.getRole());
        System.out.println("E-mail: " + user.getEmail());
        System.out.println("First Name: " + user.getFirstName());
        System.out.println("Middle Name: " + user.getMiddleName());
        System.out.println("Last Name: " + user.getLastName());
        System.out.println("Locked: " + user.isLocked());
    }

}
