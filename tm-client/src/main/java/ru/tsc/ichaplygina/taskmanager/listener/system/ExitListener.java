package ru.tsc.ichaplygina.taskmanager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.endpoint.Session;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;
import ru.tsc.ichaplygina.taskmanager.listener.AbstractListener;
import ru.tsc.ichaplygina.taskmanager.listener.domain.DomainSaveBackupListener;

@Component
public final class ExitListener extends AbstractListener {

    @NotNull
    public final static String CMD_NAME = "exit";

    @NotNull
    public final static String DESCRIPTION = "quit";

    @NotNull
    private static final String SAVE_BACKUP_COMMAND = DomainSaveBackupListener.NAME;

    @Nullable
    @Override
    public final String argument() {
        return null;
    }

    @NotNull
    @Override
    public final String command() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@exitListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        @Nullable final Session currentSession = sessionService.getSession();
        if (currentSession != null) getSessionEndpoint().closeSession(currentSession);
        System.exit(0);
    }

}
