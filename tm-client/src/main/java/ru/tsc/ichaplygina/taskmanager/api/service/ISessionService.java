package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.endpoint.Session;

public interface ISessionService {

    Session getSession();

    void setSession(Session session);

}
