package ru.tsc.ichaplygina.taskmanager.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.api.service.IDomainService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class Backup implements Runnable {

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    public void init() {
        load();
        start();
    }

    private void load() {
        domainService.loadBackup();
    }

    @Override
    @SneakyThrows
    public void run() {
        save();
    }

    private void save() {
        domainService.saveBackup();
    }

    public void start() {
        executorService.scheduleWithFixedDelay(this, 0, propertyService.getAutosaveFrequency(), TimeUnit.MILLISECONDS);
    }

}
