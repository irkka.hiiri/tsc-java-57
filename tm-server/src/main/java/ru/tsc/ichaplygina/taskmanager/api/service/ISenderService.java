package ru.tsc.ichaplygina.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.dto.EntityLogDTO;

public interface ISenderService {

    void send(@NotNull EntityLogDTO entity);

    EntityLogDTO createMessage(@NotNull Object record, @NotNull String type);

}
